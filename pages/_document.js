import { Html, Head, Main, NextScript } from "next/document"
import Script from 'next/script'
export default function Document() {
    return (
        <Html lang="en">
            <Head>
                <link
                    href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600&display=swap"
                    rel="stylesheet"
                />
                <Script src="https://app-sandbox.duitku.com/lib/js/duitku.js"></Script>
                <Script src="https://app-prod.duitku.com/lib/js/duitku.js"></Script>
                <title>Kontrak Hukum</title>
            </Head>
            <body>
            <Main />
            <NextScript />
            </body>
        </Html>
    )
}
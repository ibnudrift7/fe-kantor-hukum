import Header from "../../src/component/layout/header";
import FormOne from "../../src/component/form/form_one";
import FormTwo from "../../src/component/form/form_two";
import FormThree from "../../src/component/form/form_three";
import FormFour from "../../src/component/form/form_four";
import {useState} from "react";
import {useRouter} from "next/router";
import {backendURL} from "../../src/helpers/config";
import axios from "axios";
import $ from "jquery";
import Head from 'next/head'

const Form = (props) => {
    const router = useRouter();
    const {dataLayanan, dataLayananTambahan, diskonData} = props

    const [dataPT, setDataPT] = useState(router.query);
    const [showFormOne, setShowFormOne] = useState(true);
    const [showFormTwo, setShowFormTwo] = useState(false);
    const [showFormThree, setShowFormThree] = useState(false);
    const [showFormFour, setShowFormFour] = useState(false);
    const [paymentUrl, setPaymentUrl] = useState(null);
    const [checkBayar, setCheckBayar] = useState(false);
    const [loading, setLoading] = useState('');

    const toThankYou = () => {
        router.push("/thankyou");
    }

    const paymentDuitku = () => {
        setLoading("Loading ...")
        //axios post create payment
        axios.post(`${backendURL.baseURL}createInvoice`, {
            dataPT,
        }).then(res => {
            setPaymentUrl(res.data)
            console.log(res.data.message)
            if (res.data.message == 'thankyou'){
                toThankYou()
            }
            else{
                window.location.replace(res.data.paymentUrl)
            }
        }).catch(err => {
            console.log(err.response.data)
        })
        setLoading(null)
    }


    return (
        <>
            <Head>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            </Head>
            <Header/>
            {showFormOne &&
                <FormOne dataLayanan={dataLayanan} dataPT={dataPT} setDataPT={setDataPT} setShowFormOne={setShowFormOne}
                         setShowFormTwo={setShowFormTwo}/>}
            {showFormTwo && <FormTwo setDataPT={setDataPT} dataPT={dataPT} setShowFormOne={setShowFormOne}
                                     setShowFormTwo={setShowFormTwo} setShowFormThree={setShowFormThree}/>}
            {showFormThree && <FormThree dataPT={dataPT} setDataPT={setDataPT} setShowFormTwo={setShowFormTwo}
                                         setShowFormThree={setShowFormThree} setShowFormFour={setShowFormFour}/>}
            {showFormFour && <FormFour paymentDuitku={paymentDuitku} dataLayanan={dataLayanan}
                                       dataLayananTambahan={dataLayananTambahan} dataPT={dataPT} setDataPT={setDataPT}
                                       setShowFormThree={setShowFormThree}
                                       setCheckBayar={setCheckBayar}
                                       checkBayar={checkBayar}
                                       loading={loading}
                                       diskonData={diskonData}
            />}
        </>
    )
}

export async function getServerSideProps() {
    const res = await fetch(`${backendURL.baseURL}dataLayanan`);
    const dataLayanan = await res.json();

    const resTambahan = await fetch(`${backendURL.baseURL}dataLayananTambahan`);
    const dataLayananTambahan = await resTambahan.json();

    const diskon = await fetch(`${backendURL.baseURL}diskon`);
    const diskonData = await diskon.json();

    return {
        props: {
            dataLayanan,
            dataLayananTambahan,
            diskonData
        },
        revalidate: 10,
    }
}

export default Form;
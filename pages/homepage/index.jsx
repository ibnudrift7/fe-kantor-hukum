import {useRouter} from "next/router";

import {useState} from "react";

const Homepage = () => {
    const router = useRouter();

    const [dataPT, setDataPT] = useState([
        {}
    ])

    const inputBadanUsahaHome = (e) => {
        setDataPT({
            nama_usaha1: e.target.value
        })
    }

    const toForm = () => {
        router.push({
            pathname: "/form",
            query: dataPT.nama_usaha1 ? dataPT : {}
        });
    }

    return (
        <div>
            <div className="wrapper homepage">
                <div className="header">
                    <div className="container">
                        <nav>
                            <div className="logo-head"><img src="/logo-head.png" alt=""/></div>
                        </nav>
                    </div>
                </div>
                <div className="main_content d-flex align-content-center set_center">
                    <div className="container">
                        <div className="pos_fly_women f_mobile d-block d-lg-none">
                            <img src="/women-look.png" alt="" className="img-fluid"/>
                        </div>
                        <div className="box-wrap-form">
                            <div className="inner">
                                <div className="top-info mb-5">
                                    <h2>Pendirian Perusahaan <br/>
                                    <span>Cepat Gak Pake Ribet!</span></h2>
                                    <p>prosesnya online, jadi dalam 2 hari aja! <span>Hanya Rp 3.990.000</span></p>
                                </div>
                                <div className="forms-box">
                                    <form action="#">
                                        <div className="row g-0">
                                            <div className="col">
                                                <input type="text" onChange={inputBadanUsahaHome} className="form-control" placeholder="Apa nama PT yang ingin anda gunakan?" />
                                            </div>
                                            <div className="col-12 d-block d-lg-none">
                                                <p className="info pt-2 pb-4">*Gunakan 3 kata dengan bahasa Indonesia</p>
                                            </div>
                                            <div className="col">
                                                    <button onClick={toForm} type="button" className="btn btn-info">Buat Sekarang!</button>
                                            </div>
                                        </div>
                                    </form>
                                    <p className="info d-none d-lg-block">*Gunakan 3 kata dengan bahasa Indonesia</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="pos_fly_women d-none d-lg-block">
                        <img src="/women-look.png" alt="" className="img-fluid"/>
                    </div>
                </div>
                <div className="footer">
                    <div className="container">
                        <div className="lgo_footer">
                            <img src="/lgo-smesco.png" alt=""/>
                            <span>powered by</span>
                            <img src="/lg-kontrakhukum.png" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Homepage;
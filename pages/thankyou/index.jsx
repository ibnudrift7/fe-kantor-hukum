import Link from "next/link";

const Thankyou = () => {
    return (
        <div>
            <div className="wrapper thankyou" style={{
                backgroundImage: `url("/thankyou-circles.png")`
            }}>
                <div className="main_content set_center_full">
                    <div className="container">
                        <div className="box-wrap-info">
                            <div className="inner">
                                <div className="logo"><img src="/logokontrak-thankyou.png" className="img-fluid"/></div>
                                <div className="py-2 my-1"></div>
                                <h2>Tim Kami Akan Segera <br/><span>Menghubungi Anda</span></h2>
                                <p>Terima kasih telah mempercayakai <br className="d-block d-lg-none" />Kontrak Hukum</p>
                                <div className="py-3"></div>
                                <div className="row maw528">
                                    <div className="col col-md-6">
                                        <Link href="/form">
                                            <a href="#" className="d-inline-block btn btn-light btn-defaults_set">Submit Form Baru</a>
                                        </Link>
                                    </div>
                                    <div className="col col-md-6">
                                        <Link href="/">
                                            <a href="#" className="d-inline-block btn btn-blue btn-defaults_set">Ke Website Utama</a>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Thankyou;
import 'bootstrap/dist/css/bootstrap.css'
import '../src/styles/globals.scss'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp

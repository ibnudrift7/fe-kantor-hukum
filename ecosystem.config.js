module.exports = {
  apps : [{
    name: 'Front End Kontrak Hukum',
    script: 'node_modules/.bin/next',
    args:'start',
    cwd:"/var/www/fe-kontrak-hukum",
    instances: 3,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
        NODE_ENV: "production",
        PORT: 4000
    },
  }]
};
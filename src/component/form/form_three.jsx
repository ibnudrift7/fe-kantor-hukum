import { useRouter } from 'next/router'
import {useState} from "react";

const FormThree = (props) => {
    const router = useRouter()
    const {dataPT, setDataPT, setShowFormTwo, setShowFormThree, setShowFormFour} = props;

    const [jabatan, setJabatan] = useState([
        {
            id: 1,
            nama_jabatan: "Bukan Pengurus",
        },
        {
            id: 2,
            nama_jabatan: "Direktur Utama",
        },
        {
            id: 3,
            nama_jabatan: "Direktur",
        },
        {
            id: 4,
            nama_jabatan: "Komisaris",
        }
    ]);
    const [dataPemegangSaham, setDataPemegangSaham] = useState([
        {
            id: 1,
            jabatan: 1,
        },
    ]);

    const toFormTwo = () => {
        setShowFormTwo(true);
        setShowFormThree(false);
    }
    const toPembayaran = () => {
        setDataPT({...dataPT, dataPemegangSaham})
        setShowFormThree(false);
        setShowFormFour(true);
    }

    const tambahPengurus = () => {
        setDataPemegangSaham([
            ...dataPemegangSaham, {
            id: dataPemegangSaham.length + 1,
            jabatan: 1,
        }]);
    }

    const changeJabatan = (e) => {
        const data = e.target.value
        const id = parseInt(e.target.id.slice(7))

        setDataPemegangSaham(dataPemegangSaham.map(item => {
            if (item.id === id) {
                jabatan.map(jabatan => {
                    if (jabatan.id == data) {
                        item.jabatan = jabatan.nama_jabatan
                    }
                })
            }
            return item
        }))
    }

    const uploadKTP = (e) => {
        const file = e.target.files[0]
        const data = parseInt(e.target.id.slice(3))
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => {
            setDataPemegangSaham(dataPemegangSaham.map(item => {
                if (item.id == data) {
                    item.ktp_file = reader.result
                    item.ktp_name = file.name
                }
                return item
            }))
        }
    }

    const uploadNPWP = (e) => {
        const file = e.target.files[0]
        const data = parseInt(e.target.id.slice(4))
        const reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = () => {
            setDataPemegangSaham(dataPemegangSaham.map(item => {
                if (item.id == data) {
                    item.npwp_file = reader.result
                    item.npwp_name = file.name
                }
                return item
            }))
        }
    }

    console.log(dataPemegangSaham)

    return (
        <div>
            <div className="main-content-forms-outer bg-white">
                <div className="container">
                    <div className="inner py-5">
                        <div className="box-widget maw725 mx-auto d-block">
                            <div className="top-title pb-4 text-center">
                                <h2>Pemegang Saham Perusahaan</h2>
                            </div>
                            <div className="contents pb-3">

                                {dataPemegangSaham.map((items, index2) => {
                                    return (
                                        <div className="block-lists" key={index2}>
                                            <h6 className="small_title">Pemegang Saham {items.id}</h6>
                                            <div className="form-group mb-4 pb-3">
                                                <label className="mb-3">Peran Kepengurusan &nbsp;<img src="/icon-questions.png" alt=""/></label>
                                                <div className="row transition mb-3">
                                                    {jabatan.map((item, index) => {
                                                        return (
                                                            <div className="col-md-3 col-6" key={index}>
                                                                <div className="form-check d-inline-block me-4">
                                                                    <input className="form-check-input" type="radio" name={`jabatan${items.id}`} id={`jabatan${items.id}`} value={item.id} onChange={changeJabatan} />
                                                                    <label className="form-check-label" htmlFor={item.id}>
                                                                        {item.nama_jabatan}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        )
                                                    })
                                                    }
                                                </div>
                                                <div className="row transition">
                                                    <div className="col-md-6">
                                                        <label className="mb-2">Upload KTP</label>
                                                        <input className="form-control" onChange={uploadKTP} type="file" id={`ktp${items.id}`} />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <div className="d-block d-sm-none py-2"></div>
                                                        <label className="mb-2">Upload NPWP</label>
                                                        <input className="form-control" onChange={uploadNPWP} type="file" id={`npwp${items.id}`} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })
                                }


                                <div className="py-2 d-none d-sm-block"></div>
                                <button onClick={tambahPengurus} className="btn btn-info btns_default w-100"><img src="/ic-plus-saham.png" alt=""/> &nbsp;Tambah Pemegang Saham</button>
                                <div className="py-2"></div>
                            </div>
                            {/*end contents*/}

                            <div className="bottoms mt-3 pt-4 transition">
                                <div className="row">
                                    <div className="col-6">
                                        <button className="btn btn-info btns_default btns-back" onClick={() => toFormTwo()}>Sebelumnya</button>
                                    </div>
                                    <div className="col-6 text-end">
                                        <button className="btn btn-info btns_default" onClick={() => toPembayaran()}>Selanjutnya</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FormThree;
import {useState} from "react";

const FormOne = (props) => {
    const {dataPT,setDataPT, setShowFormOne, dataLayanan, setShowFormTwo} = props;

    const [showforms, setShowForms] = useState(false);
    const [activeClickPT, setActiveClickPT] = useState(false);
    const [activeClickCV, setActiveClickCV] = useState(false);
    const [dataBadanUsaha, setDataBadanUsaha] = useState(dataLayanan.data)

    const toFormTwo = () => {
        if (!dataPT.nama_layanan || !dataPT.nama_lengkap || !dataPT.email || !dataPT.no_tel) {
            alert("Lengkapi data anda")
        }
        else{
            setShowFormTwo(true);
            setShowFormOne(false);
        }
    }

    const clickUsahaPT = () => {
        setActiveClickPT(!activeClickPT);
        setActiveClickCV(false);
        setShowForms(true);
        setDataPT({ ...dataPT, nama_layanan: 1});
    }

    const clickUsahaCV = () => {
        setActiveClickCV(!activeClickCV);
        setActiveClickPT(false);
        setShowForms(true);
        setDataPT({ ...dataPT, nama_layanan: 2});
    }

    const inputNamaLengkap = (e) => {
        setDataPT({ ...dataPT, nama_lengkap: e.target.value});
    }

    const inputEmail = (e) => {
        setDataPT({ ...dataPT, email: e.target.value});
    }

    const inputNoTel = (e) => {
        setDataPT({ ...dataPT, no_tel: e.target.value});
    }

    return (
        <div>
            <div className="main-content-forms-outer bg-white">
                <div className="container">
                    <div className="inner py-5">
                        <div className="box-widget maw725 mx-auto d-block">
                            <div className="top-title pb-4">
                                <h4>Badan Usaha Yang ingin anda buat</h4>
                            </div>
                            <div className="contents pb-3">
                                <div className="row mb-4">
                                    <div className="col-md-6 col-6">
                                        <div
                                            className={activeClickPT ? "info-name active" : "info-name"}
                                            onClick={clickUsahaPT}>
                                            <div className="row g-2">
                                                <div className="col-md-3">
                                                    <img src="/nicon-building.png" alt=""/>
                                                </div>
                                                <div className="col d-flex align-items-center">
                                                    <span className="w-100">Pembuatan PT</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-6">
                                        <div
                                            className={activeClickCV ? "info-name active" : "info-name"}
                                            onClick={clickUsahaCV}>
                                            <div className="row g-2">
                                                <div className="col-md-3">
                                                    <img
                                                        src="/nicon-warehouses.png" alt=""/>
                                                </div>
                                                <div className="col d-flex align-items-center">
                                                    <span className="w-100">Pembuatan CV</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {showforms ?
                                    <div className="row transition mb-4">
                                        <div className="col-12">
                                            <input type="text" className="form-control" onChange={inputNamaLengkap} value={dataPT.nama_lengkap} placeholder="Nama Lengkap"/>
                                        </div>
                                    </div>
                                    : null}
                                {showforms ?
                                    <div className="row transition mb-4">
                                        <div className="col-md-6">
                                            <div className="input-group">
                                                <span className="input-group-text" id="basic-addon1"><img
                                                    src="/l-indonesia.png" alt=""/> &nbsp;INA</span>
                                                <input onChange={inputNoTel} type="tel" className="form-control" value={dataPT.no_tel} placeholder="Nomer Telepon"
                                                       aria-label="Username" aria-describedby="basic-addon1"/>
                                            </div>
                                        </div>
                                        <div className="col-md-6"><input onChange={inputEmail} type="email" value={dataPT.email} className="form-control"
                                                                         placeholder="Alamat Email"/></div>
                                    </div>
                                    : null}
                            </div>
                            {/*end contents*/}

                            {showforms ?
                                <div className="bottoms mt-3 pt-4 transition">
                                    <div className="row">
                                        <div className="col"></div>
                                        <div className="col text-end">
                                            <button className="btn btn-info btns_default"
                                                    onClick={() => toFormTwo()}>Selanjutnya
                                            </button>
                                        </div>
                                    </div>
                                </div> : null}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FormOne;
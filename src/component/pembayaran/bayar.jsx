import {useState} from "react";
import formatRupiah from "../../helpers";
import {useRouter} from "next/router";
const Bayar = (props) => {

    const router = useRouter()
    const [dataPT, setDataPT] = useState(router.query);
    console.log(dataPT)
    const [checked, setChecked] = useState(false);
    const [dataPelayanan, setDataPelayanan] = useState([
        {
            nama_pelayanan: "OSS / NIB / TDP / Notifikasi BPJS",
            price: 500000,
            image: "/nicon-start2.png"
        },
        {
            nama_pelayanan: "NPWP",
            price: 500000,
            image: "/nicon-start2.png"
        },
        {
            nama_pelayanan: "Pendaftaran Merek",
            price: 3290000,
            image: "/nicon-badges.png"
        },
        {
            nama_pelayanan: "Pengecekan Merek",
            price: 390000,
            image: "/nicon-badges.png"
        }
    ]);

    const [dataPembayaran, setDataPembayaran] = useState([
        {
            id: 1,
            nama_pembayaran: "Pendaftaran Nama PT",
            price: 3290000,
            image: "/nicon-building.png"
        }
    ]);

    const [total, setTotal] = useState(dataPembayaran.reduce((acc, curr) => acc + curr.price, 0));

    const addDataPembayaran = (data) => {
        setDataPembayaran([
            ...dataPembayaran,
            {
                id: dataPembayaran.length + 1,
                nama_pembayaran: data.nama_pelayanan,
                price: data.price,
                image: data.image
            }
        ])
        setDataPelayanan(dataPelayanan.filter(item => item.nama_pelayanan !== data.nama_pelayanan))
        setTotal(total + data.price)
    }

    const removeDataPembayaran = (data) => {
        setDataPembayaran(dataPembayaran.filter(item => item.nama_pembayaran !== data.nama_pembayaran))
        setDataPelayanan([
            ...dataPelayanan,
            {
                nama_pelayanan: data.nama_pembayaran,
                price: data.price,
                image: data.image
            }
        ])
        setTotal(total - data.price)
    }

    const reqPayment = () => {
        router.push("/thankyou");
    }

    return (
        <>
            <div className="main-content-forms-outer bg-white">
                <div className="container">
                    <div className="inner py-5">
                        <div className="row">
                            <div className="col-md-8">
                                <div className="outer_breadcrumbs">
                                    <div className="inner">
                                        <nav aria-label="breadcrumb">
                                            <ol className="breadcrumb mb-0">
                                                <li className="breadcrumb-item active"><a href="#"><span
                                                    className="numb">1</span><br
                                                    className="d-block d-sm-none"/> Pesan</a></li>
                                                <li className="breadcrumb-item"><a href="#"><span
                                                    className="numb">2</span><br
                                                    className="d-block d-sm-none"/> Pembayaran</a></li>
                                                <li className="breadcrumb-item"><a href="#"><span
                                                    className="numb">3</span><br
                                                    className="d-block d-sm-none"/> Jalankan <br
                                                    className="d-block d-sm-none"/>Proyek</a></li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                                <div className="py-2 my-1"></div>

                                <div className="card set_default p2">
                                    <div className="card-header">
                                        <h4>Informasi Pesanan</h4>
                                    </div>
                                    <div className="card-body">
                                        <ol className="list-group groups_data">
                                            {dataPembayaran.map((item, index) => {
                                                return (
                                                    <li className="list-group-item" key={index}>
                                                        <div className="me-auto p-2">
                                                            <div className="row g-0">
                                                                <div className="col-md-2">
                                                                    <div className="ms-2">
                                                                        <img src={item.image} alt=""
                                                                             className="img img-fluid"/>
                                                                    </div>
                                                                </div>
                                                                <div className="col">
                                                                    <span
                                                                        className="names mb-1">{item.nama_pembayaran}</span>
                                                                    <p className="prices mb-0">{formatRupiah(item.price)}</p>
                                                                </div>
                                                                {item.id === 1 ?
                                                                    null :
                                                                    <div className="col-md-3 text-end my-auto">
                                                                        <button className="btn btn-link p-0"><img
                                                                            src="/nicon-delete.png"
                                                                            onClick={() => removeDataPembayaran(item)}
                                                                            alt="" className="img img-fluid"/></button>
                                                                    </div>
                                                                }

                                                            </div>
                                                        </div>
                                                    </li>
                                                )
                                            })
                                            }
                                        </ol>
                                        <div className="clear"></div>
                                    </div>
                                </div>
                                <div className="py-2 my-1"></div>
                                <div className="card set_default p2">
                                    <div className="card-header">
                                        <h4>Tambahkan Layanan</h4>
                                    </div>
                                    <div className="card-body">
                                        <ol className="list-group groups_data">
                                            {dataPelayanan.map((item, index) => {
                                                return (
                                                    <li className="list-group-item" key={index}>
                                                        <div className="me-auto p-2">
                                                            <div className="row g-0">
                                                                <div className="col-md-2">
                                                                    <div className="ms-2">
                                                                        <img src={item.image} alt=""
                                                                             className="img img-fluid"/>
                                                                    </div>
                                                                </div>
                                                                <div className="col">
                                                                    <span
                                                                        className="names mb-1">{item.nama_pelayanan}</span>
                                                                    <p className="prices mb-0">{formatRupiah(item.price)}</p>
                                                                </div>
                                                                <div className="col-md-3 text-end">
                                                                    <button onClick={() => addDataPembayaran(item)}
                                                                            className="btn btn-link btns_def_link2"><img
                                                                        src="/nicon-plus.png" alt=""
                                                                        className="img img-fluid"/> &nbsp;Tambahkan
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                )
                                            })
                                            }
                                        </ol>
                                        <div className="clear"></div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="card set_default ringkasan">
                                    <div className="card-header">
                                        <h4>Ringkasan Pesan</h4>
                                    </div>
                                    <div className="card-body">
                                        <table className="table borderless">
                                            <tbody>
                                            {dataPembayaran.map((item, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td>{item.nama_pembayaran}</td>
                                                        <td className="text-end">{formatRupiah(item.price)}</td>
                                                    </tr>
                                                )
                                            })
                                            }
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td><b>Total Harga</b></td>
                                                <td className="text-end"><b>{formatRupiah(total)}</b></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        <div className="form-check checks_info">
                                            <input className="form-check-input" type="checkbox" value="" onChange={() => setChecked(!checked)}/>
                                            <label className="form-check-label" htmlFor="flexCheckDefault">
                                                Saya menyetujui <a href="#">Syarat dan Ketentuan</a> serta <a
                                                href="#">Kebijakan Privasi</a> dari Kontrak Hukum
                                            </label>
                                        </div>
                                        <div className="py-2"></div>
                                        <button onClick={reqPayment} type="submit" className={checked ? "btn btn-blue btn-defaults_set2" : "btn btn-grey btn-defaults_set2"}>Pembayaran
                                        </button>
                                        <div className="py-2"></div>
                                        <div className="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
export default Bayar;
import {useRouter} from "next/router";
import {useState} from "react";
import formatRupiah from "../../helpers";
import axios from "axios";
import {backendURL} from "../../helpers/config";

const FormFour = (props) => {
    //Hooks & props
    const router = useRouter()
    const {diskonData, loading, checkBayar, setCheckBayar, dataPT, setDataPT, dataLayanan, dataLayananTambahan, paymentDuitku} = props;
    const [checked, setChecked] = useState(checkBayar);
    const [dataPelayanan, setDataPelayanan] = useState(dataLayananTambahan.data);
    const [dataPembayaran, setDataPembayaran] = useState(dataLayanan.data.filter(item => item.id === dataPT.nama_layanan));
    const [total, setTotal] = useState(dataPembayaran.reduce((acc, curr) => acc + curr.price, 0));
    const [diskon, setDiskon] = useState();
    const [labelDiskon, setLabelDiskon] = useState("");
    const [diskonStatus, setDiskonStatus] = useState(false);
    const [diskonName, setDiskonName] = useState("");
    const [diskonValue, setDiskonValue] = useState(0);
    const [diskonType, setDiskonType] = useState("");

    //Functions
    const addDataPembayaran = (data) => {
        setDataPembayaran([
            ...dataPembayaran,
            {
                id: dataPembayaran.length + 1,
                nama_layanan: data.nama_layanan_tambahan,
                price: data.price,
                image: data.image
            }
        ])
        setDataPelayanan(dataPelayanan.filter(item => item.nama_layanan_tambahan !== data.nama_layanan_tambahan))
        setTotal(total + data.price)
        setDataPT({...dataPT, dataPembayaran, total})
        setChecked(false)
    }

    const removeDataPembayaran = (data) => {
        setDataPembayaran(dataPembayaran.filter(item => item.nama_layanan !== data.nama_layanan))
        setDataPelayanan([
            ...dataPelayanan,
            {
                nama_layanan_tambahan: data.nama_layanan,
                price: data.price,
                image: data.image
            }
        ])
        setTotal(total - data.price)
        setDataPT({...dataPT, dataPembayaran, total})
        setChecked(false)
    }

    const reqPayment = () => {
        // router.push("/thankyou");
        paymentDuitku()
        setCheckBayar(false)
    }

    const checkedPayment = () => {
        setDataPT({...dataPT, dataPembayaran, total, diskonValue, diskonName})
        setChecked(!checked)
        setCheckBayar(true)
    }

    const diskonText = (e) => {
        setDiskon(e.target.value)
    }

    const applyDiskon = () => {
        axios.post(`${backendURL.baseURL}applyDiskon`, {
            diskon_name:diskon,
        }).then(res => {
            setLabelDiskon(res.data.message)
            setDiskonStatus(true)
            setDiskonName(res.data.data.diskon_name)
            setDiskonValue(res.data.data.diskon_value)
            setDiskonType(res.data.data.diskon_type)
            if (res.data.data.diskon_type === "percent") {
                setTotal(total - (total * (parseInt(res.data.data.diskon_value) / 100)))
            }else{
                setTotal(total - parseInt(res.data.data.diskon_value))
            }
        }).catch(err => {
            setLabelDiskon("Diskon Voucher tidak ditemukan")
            setDiskonStatus(false)
            setDiskonName(null)
            setDiskonValue(0)
            setDiskonType(null)
            setTotal(dataPembayaran.reduce((acc, curr) => acc + curr.price, 0))
        })
        setChecked(false)
    }

    //Render
    return (
        <>
            <div className="main-content-forms-outer bg-white">
                <div className="container">
                    <div className="inner py-5">
                        <div className="row">
                            <div className="col-md-8">
                                <div className="outer_breadcrumbs">
                                    <div className="inner">
                                        <nav aria-label="breadcrumb">
                                            <ol className="breadcrumb mb-0">
                                                <li className="breadcrumb-item active"><a href="#"><span
                                                    className="numb">1</span><br
                                                    className="d-block d-sm-none"/> Pesan</a></li>
                                                <li className="breadcrumb-item"><a href="#"><span
                                                    className="numb">2</span><br
                                                    className="d-block d-sm-none"/> Pembayaran</a></li>
                                                <li className="breadcrumb-item"><a href="#"><span
                                                    className="numb">3</span><br
                                                    className="d-block d-sm-none"/> Jalankan <br
                                                    className="d-block d-sm-none"/>Proyek</a></li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                                <div className="py-2 my-1"></div>

                                <div className="card set_default p2">
                                    <div className="card-header">
                                        <h4>Informasi Pesanan</h4>
                                    </div>
                                    <div className="card-body">
                                        <ol className="list-group groups_data">
                                            {dataPembayaran.map((item, index) => {
                                                return (
                                                    <li className="list-group-item" key={index}>
                                                        <div className="me-auto p-2">
                                                            <div className="row g-0">
                                                                <div className="col-md-2">
                                                                    <div className="ms-2">
                                                                        <img src={item.image} alt=""
                                                                             className="img img-fluid"/>
                                                                    </div>
                                                                </div>
                                                                <div className="col">
                                                                    <span
                                                                        className="names mb-1">{item.nama_layanan}</span>
                                                                    <p className="prices mb-0">{formatRupiah(item.price)}</p>
                                                                </div>
                                                                {item.nama_layanan == "Pendaftaran Nama CV" ||item.nama_layanan == "Pendaftaran Nama PT"?
                                                                    null :
                                                                    <div className="col-md-3 text-end my-auto">
                                                                        <button className="btn btn-link p-0"><img
                                                                            src="/nicon-delete.png"
                                                                            onClick={() => removeDataPembayaran(item)}
                                                                            alt="" className="img img-fluid"/></button>
                                                                    </div>
                                                                }

                                                            </div>
                                                        </div>
                                                    </li>
                                                )
                                            })
                                            }
                                        </ol>
                                        <div className="clear"></div>
                                    </div>
                                </div>
                                <div className="py-2 my-1"></div>
                                <div className="card set_default p2">
                                    <div className="card-header">
                                        <h4>Tambahkan Layanan</h4>
                                    </div>
                                    <div className="card-body">
                                        <ol className="list-group groups_data">
                                            {dataPelayanan.map((item, index) => {
                                                return (
                                                    <li className="list-group-item" key={index}>
                                                        <div className="me-auto p-2">
                                                            <div className="row g-0">
                                                                <div className="col-md-2">
                                                                    <div className="ms-2">
                                                                        <img src={item.image} alt=""
                                                                             className="img img-fluid"/>
                                                                    </div>
                                                                </div>
                                                                <div className="col">
                                                                    <span
                                                                        className="names mb-1">{item.nama_layanan_tambahan}</span>
                                                                    <p className="prices mb-0">{formatRupiah(item.price)}</p>
                                                                </div>
                                                                <div className="col-md-3 text-end">
                                                                    <button onClick={() => addDataPembayaran(item)}
                                                                            className="btn btn-link btns_def_link2"><img
                                                                        src="/nicon-plus.png" alt=""
                                                                        className="img img-fluid"/> &nbsp;Tambahkan
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                )
                                            })
                                            }
                                        </ol>
                                        <div className="clear"></div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-4">
                                <div className="card set_default ringkasan">
                                    <div className="card-header">
                                        <h4>Ringkasan Pesan</h4>
                                    </div>
                                    <div className="card-body">
                                        <div className="box-voucher mb-3 border-bottom-1">
                                            <div className="row g-2">
                                                <div className="col-md-9">
                                                    <input type="text" placeholder="Kode Voucher..." onChange={diskonText} className="form-control"/>
                                                </div>
                                                <div className="col-md-3">
                                                    <button onClick={applyDiskon} className="btn btn-blue btn-defaults_set2 w-100 h-100">Cek</button>
                                                </div>
                                            </div>
                                            {labelDiskon ? <div className={diskonStatus ? "alert mt-3 alert-success py-1 fs-6" : "alert mt-3 alert-warning py-1 fs-6"}  >
                                                <p className="m-0"><small>{labelDiskon ? labelDiskon : null}</small></p>
                                            </div> : null}

                                        </div>

                                        <table className="table borderless">
                                            <tbody>
                                            {dataPembayaran.map((item, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td>{item.nama_layanan}</td>
                                                        <td className="text-end">{formatRupiah(item.price)}</td>
                                                    </tr>
                                                )
                                            })
                                            }
                                            {diskonName ?
                                                <tr>
                                                    <td>{diskonName}</td>
                                                    <td className="text-end">{diskonValue}%</td>
                                                </tr>
                                                : null}
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td><b>Total Harga</b></td>
                                                <td className="text-end"><b>{formatRupiah(total)}</b></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        <div className="form-check checks_info">
                                            <input className="form-check-input" type="checkbox" value={checked} checked={checked} onChange={() => checkedPayment()}/>
                                            <label className="form-check-label" htmlFor="flexCheckDefault">
                                                Saya menyetujui <a href="#">Syarat dan Ketentuan</a> serta <a
                                                href="#">Kebijakan Privasi</a> dari Kontrak Hukum
                                            </label>
                                        </div>
                                        <div className="py-2"></div>
                                        <button onClick={reqPayment} type="submit" className={checked ? "btn btn-blue btn-defaults_set2" : "btn btn-grey btn-defaults_set2"}>{loading ? loading : "Pembayaran"}</button>

                                        <div className="py-2"></div>
                                        <div className="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
export default FormFour;
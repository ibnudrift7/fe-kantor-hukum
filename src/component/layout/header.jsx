const Header = () => {
    return (
        <div>
            <header className="insides">
                <div className="container">
                    <div className="row">
                        <div className="col-5">
                            <div className="backs_btns pt-2">
                                {/* eslint-disable-next-line @next/next/no-html-link-for-pages */}
                                <a href="/" className="btn btn-link">
                                    <img src="/to_back.png"  alt="" className="img-fluid pe-3"/>Kembali</a>
                            </div>
                        </div>
                        <div className="col">
                            <div className="text-end logo_head"><a href="#" className="btn btn-link p-0">
                                <img src="/logo-head.png" className="img img-fluid"/></a></div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    );
}

export default Header;
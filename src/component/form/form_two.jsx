const FormTwo = (props) => {
    const {dataPT,setDataPT, setShowFormOne, setShowFormTwo, setShowFormThree} = props;
    const toFormThree = () => {
        if (!dataPT.nama_usaha1) {
            alert("Lengkapi data anda")
        }
        else{
            setShowFormThree(true);
            setShowFormTwo(false);
        }
    }
    const toFormOne = () => {
        setShowFormOne(true);
        setShowFormTwo(false);
    }

    const setNamaUsaha1 = (e) => {
        setDataPT({ ...dataPT, nama_usaha1: e.target.value});
    }
    const setNamaUsaha2 = (e) => {
        setDataPT({ ...dataPT, nama_usaha2: e.target.value});
    }
    const setNamaUsaha3 = (e) => {
        setDataPT({ ...dataPT, nama_usaha3: e.target.value});
    }
    return (
        <div>
            <div className="main-content-forms-outer bg-white">
                <div className="container">
                    <div className="inner py-5">
                        <div className="box-widget maw725 mx-auto d-block">
                            <div className="top-title pb-4">
                                <h4>Nama Badan Usaha</h4>
                                <p>Beri nama untuk perusahaan anda dengan 3 kata dan mengandung bahasa indonesia</p>
                            </div>
                            <div className="contents pb-3">

                                    <div className="row transition mb-4">
                                        <div className="col-12">
                                            <input type="text" onChange={setNamaUsaha1} value={dataPT.nama_usaha1} className="form-control" placeholder="Nama Badan Usaha"/>
                                        </div>
                                    </div>
                                    <div className="row transition mb-4">
                                        <div className="col-md-6">
                                            <div className="input-group">
                                                <input type="text" onChange={setNamaUsaha2} value={dataPT.nama_usaha2} className="form-control border-end-0" placeholder="Nama alternatif ke -1"
                                                       aria-label="Username" aria-describedby="basic-addon1" />
                                                <span className="input-group-text disable-back" id="basic-addon1"><img src="/icon-questions.png" alt=""/></span>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="input-group">
                                                <input type="text" onChange={setNamaUsaha3} value={dataPT.nama_usaha3} className="form-control border-end-0" placeholder="Nama alternatif ke -2"
                                                       aria-label="Username" aria-describedby="basic-addon1" />
                                                <span className="input-group-text disable-back" id="basic-addon1"><img src="/icon-questions.png" alt=""/></span>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            {/*end contents*/}

                                <div className="bottoms mt-3 pt-4 transition">
                                    <div className="row">
                                        <div className="col-6">
                                            <button className="btn btn-info btns_default btns-back" onClick={() => toFormOne()}>Sebelumnya</button>
                                        </div>
                                        <div className="col-6 text-end">
                                            <button className="btn btn-info btns_default" onClick={() => toFormThree()}>Selanjutnya</button>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default FormTwo;
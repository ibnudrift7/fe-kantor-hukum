import Header from "../../src/component/layout/header";
import Bayar from "../../src/component/pembayaran/bayar";

const Pembayaran = () => {
    return (
        <>
            <Header />
            <Bayar />
        </>
    );
}

export default Pembayaran;